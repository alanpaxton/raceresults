package com.isomaly.rr.raceresults;

import android.*;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Simple/pretendy results connector subclass using email.
 *
 * Created by alan on 17/11/2016.
 */

@Singleton
public class ResultsConnectorEmail extends ResultsConnector {

    @Inject
    public ResultsConnectorEmail() {
    }

    /**
     * Check for an already registered stored password
     *
     * @return required permissions
     *
     * @param context
     */
    @Override
    protected List<String> loginOrRegister(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                BuildConfig.shared_preferences_key, Context.MODE_PRIVATE);
        String authKey = BuildConfig.firebase_auth_key;
        if (sharedPreferences.contains(authKey)) {
            state = State.Login;
            login(getEmail(), sharedPreferences.getString(authKey, ""));
        } else {
            //Generate a password
            int permission = ContextCompat.checkSelfPermission(context,
                    android.Manifest.permission.GET_ACCOUNTS);
            if (PackageManager.PERMISSION_GRANTED == permission)
            {
                state = State.Register;
                register(getEmail(), UUID.randomUUID().toString());
            } else {
                Log.w(TAG, "loginOrRegister GET_ACCOUNTS permission was not granted: " + permission);
                //Return list of the single permission we need
                return Collections.singletonList(android.Manifest.permission.GET_ACCOUNTS);
            }
        }
        //List of further permissions required is empty
        return Collections.EMPTY_LIST;
    }

    private void login(String email, String password) {

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "login:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "login", task.getException());
                            Toast.makeText(context, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "login:onFailure:", e);
            }
        });
    }

    private void register(String email, final String password) {

        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (task.isSuccessful()) {
                            //Store the registered user
                            SharedPreferences sharedPreferences = context.getSharedPreferences(
                                    BuildConfig.shared_preferences_key, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(BuildConfig.firebase_auth_key, password);
                            editor.apply();
                        } else {
                            Toast.makeText(context, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "createUserWithEmail:onFailure:", e);
            }
        });
    }

    private String getEmail() {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            if (account.type.startsWith("com.google") && emailPattern.matcher(account.name).matches()) {
                return account.name;
            }
        }
        return null;
    }

}
