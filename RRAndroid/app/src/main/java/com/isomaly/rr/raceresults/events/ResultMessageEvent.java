package com.isomaly.rr.raceresults.events;

import java.util.Map;

/**
 * Message created from notification which tells the results fragment which result has changed.
 * <p>
 *     The fragment can therefore highlight that result.
 * </p>
 * Created by alan on 09/09/2016.
 */
public class ResultMessageEvent {
    public final String body;
    public final String title;
    public final Map<String, String> data;

    public ResultMessageEvent(final String title, final String body, final Map<String, String> data) {
        this.title = title;
        this.body = body;
        this.data = data;
    }

    public String getLabel() {
        if (data != null && data.containsKey("label")) {
            return data.get("label");
        }
        return null;
    }
}
