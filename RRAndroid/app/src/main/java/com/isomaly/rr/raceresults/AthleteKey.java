package com.isomaly.rr.raceresults;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by alan on 07/09/2016.
 */
public class AthleteKey {

    private final static String TAG = "RR.AthleteKey";

    DatabaseReference athleteKeyRef;
    ValueEventListener athleteKeyEventListener;

    private final DBRef dbRef;

    public AthleteKey(DBRef dbRef) {
        this.dbRef = dbRef;
    }

    private void removeAthleteKeyRef() {
        if (athleteKeyRef != null && athleteKeyEventListener != null) {
            athleteKeyRef.removeEventListener(athleteKeyEventListener);
            athleteKeyRef = null;
            athleteKeyEventListener = null;
        }
    }

    /**
     * Listen to signin to firebase.
     */
    public static interface AccessListener {
        public void referenceReceived(DatabaseReference athleteReference);
        public void referenceUnavailable();
    }

    /**
     * Listen to the athlete key until this listen is cancelled.
     *
     * @param firebaseDatabase
     * @param athleteName
     * @param accessListener
     */
    public void listen(final FirebaseDatabase firebaseDatabase, final String athleteName, final AccessListener accessListener) {

        removeAthleteKeyRef();
        athleteKeyRef = DBRef.COMPETITOR_KEYS.ref(firebaseDatabase).child(athleteName);
        athleteKeyEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Object value = dataSnapshot.getValue();
                if (value == null) {
                    //DB may be being recreated. Expect to be updated later.
                    accessListener.referenceUnavailable();
                    return;
                }
                final DatabaseReference competitors = dbRef.ref(firebaseDatabase);
                if (competitors == null) {
                    //DB may be being recreated. Expect to be updated later.
                    accessListener.referenceUnavailable();
                    return;
                }

                String competitorId = value.toString();
                DatabaseReference athleteRef = competitors.child(competitorId);
                accessListener.referenceReceived(athleteRef);
                Log.d(TAG, "Athlete reference: " + athleteRef);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "Could not use DB reference. DB access problem: " + databaseError);
            }
        };
        athleteKeyRef.addValueEventListener(athleteKeyEventListener);
    }

    /**
     * Cancel listening to this athlete key.
     */
    public void cancel() {
        removeAthleteKeyRef();
    }
}
