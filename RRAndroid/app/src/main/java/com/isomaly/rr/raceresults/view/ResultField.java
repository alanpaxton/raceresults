package com.isomaly.rr.raceresults.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.isomaly.rr.raceresults.R;

/**
 * Created by alan on 25/08/2016.
 */
public class ResultField extends RelativeLayout {

    private String text;
    private TextView textView;
    private String units;
    private TextView unitsPositionView;
    private String label;
    private TextView labelView;
    private boolean position;

    public ResultField(Context context) {
        super(context);
        init(null, 0);
    }

    public ResultField(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ResultField(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {

        final TypedArray a = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ResultField,
                defStyle, 0);

        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutInflater.inflate(R.layout.view_result_field, this);

        try {
            text = a.getString(R.styleable.ResultField_text);
            if (TextUtils.isEmpty(text)) text = "";
            units = a.getString(R.styleable.ResultField_units);
            if (TextUtils.isEmpty(units)) units = "";
            label = a.getString(R.styleable.ResultField_label);
            if (TextUtils.isEmpty(label)) label = "";
            position = a.getBoolean(R.styleable.ResultField_position, false);
        } finally {
            a.recycle();
        }

        textView = (TextView) findViewById(R.id.ResultFieldText);
        labelView = (TextView)findViewById(R.id.ResultFieldLabel);
        unitsPositionView = (TextView)findViewById(R.id.ResultFieldUnits);

        setText(text);
        setUnits(units);
        setLabel(label);
        setPosition(position);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        textView.setText(text);
        if (position) {
            setUnitsPosition(getPositionSuffix(text));
        } else {
            setUnitsPosition(units);
        }
        invalidate();
        requestLayout();
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    private void setUnitsPosition(String text) {
        unitsPositionView.setText(text);
        if (TextUtils.isEmpty(text)) {
            unitsPositionView.setVisibility(GONE);
        } else {
            unitsPositionView.setVisibility(VISIBLE);
        }
        invalidate();
        requestLayout();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
        labelView.setText(label);
        if (TextUtils.isEmpty(label)) {
            labelView.setVisibility(GONE);
        } else {
            labelView.setVisibility(VISIBLE);
        }
        invalidate();
        requestLayout();
    }

    public boolean getPosition() {
        return position;
    }

    public void setPosition(boolean position) {
        this.position = position;
        if (position) {
            setUnitsPosition(getPositionSuffix(text));
        } else {
            setUnitsPosition(units);
        }
    }

    /**
     * The correct suffix (st for 1st, nd for 2nd, rd for 3rd, th for 4th, 5th,...)
     *
     * @param positionText the text, which we do this for if it's a number.
     * @return
     */
    private String getPositionSuffix(String positionText) {

        int position = 0;
        try {
            position = Integer.valueOf(positionText);
        } catch (NumberFormatException e) {
            return "";
        }

        if (position >= 11 && position <= 13) {
            return "th";
        }
        switch (position % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }
}
