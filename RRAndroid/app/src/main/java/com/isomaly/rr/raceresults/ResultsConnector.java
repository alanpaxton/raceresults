package com.isomaly.rr.raceresults;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Do a toy login/register with Firebase.
 *
 * Created by alan on 15/07/2016.
 */
public abstract class ResultsConnector {

    protected final static String TAG = "RR.Results";

    protected Context context;
    protected FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    enum State { Initial, Login, Register };
    State state = State.Initial;

    /**
     * Start to connect to results
     * If we need to hand in some permissions, return the list.
     *
     * @param context
     * @return the list of required permissions
     */
    public List<String> start(Context context) {
        if (firebaseAuth == null) {
            this.context = context;
            createAuthStateListener();
        }
        if (state == State.Initial) {
            return loginOrRegister(context);
        }
        else return Collections.EMPTY_LIST;
    }

    /**
     * Auth state listener which in turn triggers our own signin listeners
     */
    private void createAuthStateListener() {

        if (firebaseAuth == null) {
            firebaseAuth = FirebaseAuth.getInstance();
            authStateListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    if (user != null) {
                        // User is signed in
                        Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                        if (!user.getUid().equals(signedInUID)) {
                            signedInUID = user.getUid();
                            for (SigninListener signinListener : signinListeners) {
                                signinListener.onUserSignedIn(user);
                            }
                        }
                    } else {
                        // User is signed out
                        Log.d(TAG, "onAuthStateChanged:signed_out");
                        signedInUID = null;
                    }
                    // ...
                }
            };
            firebaseAuth.addAuthStateListener(authStateListener);
        }
    }

    public void stop() {
        if (firebaseAuth != null) {
            firebaseAuth.removeAuthStateListener(authStateListener);
            firebaseAuth = null;
        }
    }

    protected abstract List<String> loginOrRegister(Context context);

    /**
     * Listen to signin to firebase.
     */
    public static interface SigninListener {
        public void onUserSignedIn(FirebaseUser firebaseUser);
    }

    private String signedInUID;

    private final List<SigninListener> signinListeners = new ArrayList<>();

    public void addSigninListener(SigninListener listener) {
        signinListeners.add(listener);

        //Call the listener if we're already signed in.
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            listener.onUserSignedIn(user);
        }
    }

    public void removeSigninListener(SigninListener listener) {
        signinListeners.remove(listener);
    }

    public void clearSigninListeners() {
        signinListeners.clear();
    }
}
