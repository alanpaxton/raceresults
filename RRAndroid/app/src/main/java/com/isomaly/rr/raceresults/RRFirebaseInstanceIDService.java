package com.isomaly.rr.raceresults;

import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;

/**
 * @link https://firebase.google.com/docs/cloud-messaging/android/client
 * <p>
 *     Access the device registration token
 * </p>
 *
 * Created by alan on 01/09/2016.
 */
public class RRFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private final static String TAG = "RR.InstanceId";

    @Inject NotificationState notificationState;

    /**
     * Override creation to do dependency injection
     */
    @Override public void onCreate() {
        super.onCreate();

        ((RRApplication) getApplication()).getComponent().inject(this);
    }

    public void handleIntent(Intent intent) {
        notificationState.checkAndUpdate(this);
    }

    /**
     * The FCM token for this app has changed.
     * <p>
     *     We need to store/keep it where the server can see it to send
     * </p>
     */
    @Override
    public void onTokenRefresh() {

        notificationState.checkAndUpdate(this);
    }
}
