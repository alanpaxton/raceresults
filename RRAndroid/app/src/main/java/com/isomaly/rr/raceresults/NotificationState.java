package com.isomaly.rr.raceresults;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by alan on 07/09/2016.
 */
@Singleton
public class NotificationState {

    private final static String TAG = "RR.InstanceId";

    @Inject
    ResultsConnectorEmail resultsConnector;

    AthleteKey athleteKey = new AthleteKey(DBRef.COMPETITOR_SETTINGS);

    @Inject
    public NotificationState() {

    }

    /**
     * The FCM token for this app has changed.
     * <p>
     *     We need to store/keep it where the server can see it to use to send notifications back to us
     * </p>
     */
    public void checkAndUpdate(Context context) {
        // Get current (possibly updated) InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // We are keeping the registration token in Firebase, for the use of the "server code"
        // Thus "our app server" is Firebase, or rather the server code that uses it.
        updateNotificationToken(context, refreshedToken);
    }

    /**
     * Save the new notification token, check it has indeed changed, trigger necessary updates.
     * @param token
     */
    private void updateNotificationToken(Context context, String token) {

        saveUpdatedNotificationToken(context, token);
        checkNotificationTokenAndUpdate(context);
    }

    /**
     * Check the token we know is safely sent to the server is the same as the one we want to be current.
     * <p>
     *     If the one we want to be current (the updated one) is different, send it..
     * </p>
     */
    private void checkNotificationTokenAndUpdate(Context context) {

        SharedPreferences sharedPreferences =
                context.getSharedPreferences(context.getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
        String currentToken =
                sharedPreferences.getString(context.getString(R.string.preference_current_notification_token), null);
        String updatedToken =
                sharedPreferences.getString(context.getString(R.string.preference_updated_notification_token), null);
        if (!TextUtils.isEmpty(updatedToken) && !updatedToken.equals(currentToken)) {
            sendNotificationTokenToServer(context, updatedToken);
        }
    }

    private void saveUpdatedNotificationToken(Context context, String token) {
        SharedPreferences.Editor editor =
                context.getSharedPreferences(context.getString(R.string.preferences_file_name), Context.MODE_PRIVATE).edit();
        editor.putString(context.getString(R.string.preference_updated_notification_token), token);
        editor.commit();
    }

    private void saveCurrentNotificationToken(Context context, String token) {
        SharedPreferences.Editor editor =
                context.getSharedPreferences(context.getString(R.string.preferences_file_name), Context.MODE_PRIVATE).edit();
        editor.putString(context.getString(R.string.preference_current_notification_token), token);
        editor.commit();
    }

    /**
     * Send the (updated) notification token to the server
     * <p>
     *     Save the token as the current token when the server update is complete.
     *     If this doesn't happen, we should eventually re-check on restart.
     * </p>
     * @param token
     */
    private void sendNotificationTokenToServer(final Context context, final String token) {

        final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final String athleteName = context.getString(R.string.example_results_athlete_name);
        athleteKey.listen(firebaseDatabase, athleteName, new AthleteKey.AccessListener() {
            @Override
            public void referenceReceived(DatabaseReference athleteRef) {

                if (athleteRef == null) {
                    Log.e(TAG, "Could not find athlete reference for athlete: " + athleteName);
                    return;
                }
                DBRef.NOTIFICATION_ACCESS_TOKEN.child(athleteRef).setValue(token, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError == null) {
                            saveCurrentNotificationToken(context, token);
                            //We only needed it to happen once.
                            athleteKey.cancel();
                        } else {
                            Log.w(TAG, "Could not push token to Firebase. DB access problem: " + databaseError);
                        }
                    }
                });
            }

            @Override
            public void referenceUnavailable() {
                Log.e(TAG, "Athlete reference unavailable");
            }
        });
    }


}
