package com.isomaly.rr.raceresults.data;

/**
 * Created by alan on 23/08/2016.
 */
public class CompetitorRace {

    public String label;
    public RaceResult result;

    @Override
    public String toString() {
        return "CompetitorRace{" +
                "label='" + label + '\'' +
                ", result=" + result +
                '}';
    }
}
