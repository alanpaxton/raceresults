package com.isomaly.rr.raceresults;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by alan on 22/08/2016.
 */
public class DBRef {

    private final String keyPart;

    public DBRef(String keyPart) {
        this.keyPart = keyPart;
    }

    public DatabaseReference child(DatabaseReference databaseReference) {
        return databaseReference.child(keyPart);
    }

    public DatabaseReference ref(FirebaseDatabase firebaseDatabase) {
        return firebaseDatabase.getReference().child(keyPart);
    }

    public final static DBRef COMPETITOR_KEYS = new DBRef("competitorKeys");
    public final static DBRef COMPETITORS = new DBRef("competitors");

    public final static DBRef NOTIFICATION_ACCESS_TOKEN = new DBRef("notificationAccessToken");
    public final static DBRef COMPETITOR_SETTINGS = new DBRef("competitorSettings");
}
