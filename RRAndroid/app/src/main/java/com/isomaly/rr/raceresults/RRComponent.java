package com.isomaly.rr.raceresults;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by alan on 16/07/2016.
 */
@Singleton
@Component(modules = {})

public interface RRComponent {
    void inject(RRApplication application);

    // to update the fields in your activities
    void inject(ResultsActivity activity);

    // to update fields in fragments
    void inject(ResultsFragment fragment);

    void inject(RRFirebaseInstanceIDService service);
}
