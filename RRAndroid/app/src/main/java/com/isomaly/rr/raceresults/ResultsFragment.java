package com.isomaly.rr.raceresults;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.isomaly.rr.raceresults.data.CompetitorRace;
import com.isomaly.rr.raceresults.data.RaceResult;
import com.isomaly.rr.raceresults.view.ResultField;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

/**
 * A placeholder fragment containing a simple view.
 */
public class ResultsFragment extends Fragment {

    private final static String TAG = "RR.ResultsFragment";

    @Inject
    ResultsPresenter resultsPresenter;

    private RecyclerView recyclerView;
    private ResultListAdapter adapter;
    private ProgressBar progressBar;

    public ResultsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((RRApplication) getActivity().getApplication()).getComponent().inject(this);

        resultsPresenter.onCreate(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_results, container, false);
        progressBar = (ProgressBar)view.findViewById(R.id.ResultsProgress);
        progressBar.setVisibility(View.INVISIBLE);
        recyclerView = (RecyclerView)view.findViewById(R.id.ResultsList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ResultListAdapter();
        recyclerView.setAdapter(adapter);

        return view;
    }

    public static class ResultListAdapter extends RecyclerView.Adapter<ResultListAdapter.ViewHolder> {

        private List<CompetitorRace> competitorRaceResults;
        //Find the result
        private HashMap<String, Integer> raceLabelIndices;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public static class ViewHolder extends RecyclerView.ViewHolder {

            // each data item is just a string in this case
            public ViewGroup resultLayout;
            public TextView textViewLabel;
            public ResultField resultPos;
            public ResultField resultTime;
            public View viewTriSeparator;
            public ViewGroup viewGroupTri;
            public ResultField resultSwim;
            public ResultField resultBike;
            public ResultField resultRun;

            public ViewHolder(ViewGroup view) {
                super(view);
                resultLayout = (ViewGroup) view.findViewById(R.id.ResultLayout);
                textViewLabel = (TextView)view.findViewById(R.id.ResultRowLabel);
                resultPos = (ResultField)view.findViewById(R.id.ResultRowPos);
                resultTime = (ResultField)view.findViewById(R.id.ResultRowTime);
                viewTriSeparator = view.findViewById(R.id.ResultRowTriSeparator);
                viewGroupTri = (ViewGroup)view.findViewById(R.id.ResultRowTriSpecific);
                resultSwim = (ResultField)view.findViewById(R.id.ResultRowSwim);
                resultBike = (ResultField)view.findViewById(R.id.ResultRowBike);
                resultRun = (ResultField)view.findViewById(R.id.ResultRowRun);
            }

            public void highlight() {
                ValueAnimator animation = ValueAnimator.ofFloat(0.0f, 1.0f, 0.5f, 0.0f);
                animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        float alpha = (Float)valueAnimator.getAnimatedValue();
                        int color = Color.argb((int)(alpha * 255), 0, 0, 255);
                        resultLayout.setBackgroundColor(color);
                    }
                });
                animation.setDuration(2000);
                animation.start();
            }
        }

        private void updatelabelIndices() {
            this.raceLabelIndices = new HashMap<>();
            for (int i = 0; i < competitorRaceResults.size(); i++) {
                CompetitorRace race = competitorRaceResults.get(i);
                if (!TextUtils.isEmpty(race.label)) {
                    raceLabelIndices.put(race.label, i);
                }
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public ResultListAdapter(Collection<CompetitorRace> competitorRaceResults) {
            this.competitorRaceResults = new ArrayList<>(competitorRaceResults);
            updatelabelIndices();
        }

        public ResultListAdapter() {
            this(Collections.EMPTY_LIST);
        }

        public void setResults(Collection<CompetitorRace> competitorRaceResults) {
            this.competitorRaceResults = new ArrayList<>(competitorRaceResults);
            updatelabelIndices();
            notifyDataSetChanged();
        }

        public int findIndexForLabel(String label) {
            Integer index = raceLabelIndices.get(label);
            if (index == null) {
                return -1;
            }
            return index;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public ResultListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
            // create a new view
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.result_row, parent, false);
            ViewHolder viewHolder = new ViewHolder((ViewGroup)view);
            return viewHolder;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            if (competitorRaceResults.size() > position) {
                CompetitorRace competitorRace = competitorRaceResults.get(position);
                RaceResult raceResult = competitorRace.result;
                holder.textViewLabel.setText(competitorRace.label);
                holder.resultPos.setText(raceResult.pos);
                holder.resultTime.setText(raceResult.time);

                setTriViewState(holder, raceResult);
            }
        }

        private void setTriViewState(ViewHolder holder, RaceResult raceResult) {
            if (TextUtils.isEmpty(raceResult.swim) && TextUtils.isEmpty(raceResult.bike) && TextUtils.isEmpty(raceResult.run)) {
                holder.viewTriSeparator.setVisibility(View.GONE);
                holder.viewGroupTri.setVisibility(View.GONE);
            } else {
                holder.viewTriSeparator.setVisibility(View.VISIBLE);
                holder.viewGroupTri.setVisibility(View.VISIBLE);
            }

            holder.resultSwim.setText(raceResult.swim);
            holder.resultBike.setText(raceResult.bike);
            holder.resultRun.setText(raceResult.run);
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return competitorRaceResults.size();
        }
    }

    @Override public void onStart() {
        super.onStart();
        resultsPresenter.onStart();
    }

    @Override public void onResume() {
        super.onResume();
        resultsPresenter.onResume();
    }

    @Override public void onPause() {
        super.onPause();
        resultsPresenter.onPause();
    }

    @Override public void onStop() {
        super.onStop();
        resultsPresenter.onStop();
    }

    /**
     * Replace the list being displayed with a new list.
     *
     * @param competitorRaces
     */
    public void setResults(Collection<CompetitorRace> competitorRaces) {
        for (CompetitorRace competitorRace : competitorRaces) {
            Log.d(TAG, "result: " + competitorRace);
        }
        ((ResultListAdapter)recyclerView.getAdapter()).setResults(competitorRaces);
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    public void setBusy(boolean busy) {
        progressBar.setVisibility(busy ? View.VISIBLE : View.INVISIBLE);
    }

    public void setRecent(String label) {
        Log.d(TAG, "Recent " + label);
        int i = adapter.findIndexForLabel(label);
        if (i >= 0) {
            ResultListAdapter.ViewHolder viewHolder = (ResultListAdapter.ViewHolder) recyclerView.findViewHolderForAdapterPosition(i);
            if (viewHolder != null) {
                viewHolder.highlight();
            }
        }
    }
}
