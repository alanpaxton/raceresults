package com.isomaly.rr.raceresults.data;

/**
 * Created by alan on 23/08/2016.
 */
public class RaceResult {

    public String name;
    public String firstname;
    public String lastname;
    public String pos;
    public String time;
    public String number;
    public String bike;
    public String category;
    public String catpos;
    public String t1;
    public String t2;
    public String gender;
    public String genderpos;
    public String tod;
    public String age;
    public String run;
    public String swim;
    public String club;

    @Override
    public String toString() {
        return "RaceResult{" +
                "name='" + name + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", pos='" + pos + '\'' +
                ", time='" + time + '\'' +
                ", number='" + number + '\'' +
                ", bike='" + bike + '\'' +
                ", category='" + category + '\'' +
                ", catpos='" + catpos + '\'' +
                ", t1='" + t1 + '\'' +
                ", t2='" + t2 + '\'' +
                ", gender='" + gender + '\'' +
                ", genderpos='" + genderpos + '\'' +
                ", tod='" + tod + '\'' +
                ", age='" + age + '\'' +
                ", run='" + run + '\'' +
                ", swim='" + swim + '\'' +
                ", club='" + club + '\'' +
                '}';
    }
}
