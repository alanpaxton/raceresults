package com.isomaly.rr.raceresults;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.isomaly.rr.raceresults.events.ResultMessageEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

/**
 * Created by alan on 01/09/2016.
 */
public class RRFirebaseMessagingService extends FirebaseMessagingService {

    /**
     * Handle a message when the app is in the foreground
     *
     * @param message
     */
    @Override
    public void onMessageReceived (RemoteMessage message) {
        RemoteMessage.Notification notification = message.getNotification();
        if (notification != null) {
            handleNotification(notification, message);
        }
    }

    /**
     * Handle notification when we are in foreground.
     *
     * @param notification
     * @param message
     */
    private void handleNotification(RemoteMessage.Notification notification, RemoteMessage message) {

        String title = notification.getTitle();
        String body = notification.getBody();
        Map<String, String> data = message.getData();

        /*
        String[] args = notification.getBodyLocalizationArgs();
        String key = notification.getBodyLocalizationKey();
        String action = notification.getClickAction();
        String color = notification.getColor();
        String icon = notification.getIcon();
        String sound = notification.getSound();
        */

        //This is the most recent/changed result, it sticks around waiting for a recipient to highlight it.
        EventBus.getDefault().postSticky(new ResultMessageEvent(title, body, data));
    }
}
