package com.isomaly.rr.raceresults;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.List;

import javax.inject.Inject;

public class ResultsActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private final static String TAG = "RR.Results";

    public final static int RR_PERMISSIONS_REQUEST_RESULTS_CONNECTOR = 1;
    public final static int RR_UPDATE_PLAY_SERVICES_DIALOG_CODE = 2;
    public final static int RR_UPDATE_PLAY_SERVICES_INTENT_CODE = 3;

    private boolean started = false;
    private PendingIntent pendingIntent;

    @Inject public ResultsConnectorEmail resultsConnector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((RRApplication) getApplication()).getComponent().inject(this);

        setContentView(R.layout.activity_results);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        updatePlayServices();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        started = true;
        updatePlayServices();
    }

    private void updatePlayServices() {
        GoogleApiAvailability availability = GoogleApiAvailability.getInstance();
        int status = availability.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            startResultsConnector();
        } else if (availability.isUserResolvableError(status)) {
            pendingIntent = availability.getErrorResolutionPendingIntent(this, status, RR_UPDATE_PLAY_SERVICES_INTENT_CODE);
            Dialog dialog = availability.getErrorDialog(this, status, RR_UPDATE_PLAY_SERVICES_DIALOG_CODE);
            dialog.show();
        }
    }

    /**
     * Start the results connector when we're started.
     * <p>
     *     If it comes back and says it needs permissions,
     *     we initiate that, then try again to start it when we have the permissions.
     *     We can safely start the RC multiple times, it should just no-op appropriately
     *     if it is already started.
     * </p>
     */
    private void startResultsConnector() {

        List<String> permissions = resultsConnector.start(this);
        if (permissions.size() > 1) {
            Log.e(TAG, "Unexpected multiple permissions required: " + permissions);
        } else if (permissions.size() == 1) {
            requestPermission(permissions.get(0));
        }
    }

    private void requestPermission(String permission) {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                permission)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    permission)) {

                final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                        .findViewById(android.R.id.content)).getChildAt(0);

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Snackbar.make(
                        viewGroup,
                        "RR needs account permission to generate a sensible Firebase login",
                        Snackbar.LENGTH_INDEFINITE);

                //And request the permission anyway
                //Not terribly polite, but will do in a demo
                ActivityCompat.requestPermissions(this,
                        new String[]{permission},
                        RR_PERMISSIONS_REQUEST_RESULTS_CONNECTOR);

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{permission},
                        RR_PERMISSIONS_REQUEST_RESULTS_CONNECTOR);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onStop() {
        started = false;
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult (int requestCode,
                                  int resultCode,
                                  Intent data)
    {
        if (RR_UPDATE_PLAY_SERVICES_DIALOG_CODE == requestCode) {
            try {
                pendingIntent.send(null, RR_UPDATE_PLAY_SERVICES_INTENT_CODE, null, new PendingIntent.OnFinished() {

                    @Override
                    public void onSendFinished(PendingIntent pendingIntent, Intent intent, int i, String s, Bundle bundle) {
                        Log.d(TAG, "PendingIntent.onSendFinished()");
                    }
                }, null);
            } catch (PendingIntent.CanceledException e) {
                Log.e(TAG, "Update play services intent cancelled", e);
            }
        } else if (RR_UPDATE_PLAY_SERVICES_INTENT_CODE == requestCode) {
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult() " + requestCode + "[" + grantResults.length + "]");
        switch (requestCode) {
            case RR_PERMISSIONS_REQUEST_RESULTS_CONNECTOR: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. We should do the starting all over again.
                    if (started && !isFinishing()) {
                        startResultsConnector();
                    }
                } else {
                    //So we can't do what we need to do; permission denied. Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "onRequestPermissionsResult user did not grant GET_ACCOUNTS, can't log in, just hanging.");
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
