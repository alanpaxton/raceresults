package com.isomaly.rr.raceresults;

import android.text.TextUtils;
import android.util.Log;


import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.isomaly.rr.raceresults.data.CompetitorRace;
import com.isomaly.rr.raceresults.events.ResultMessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Map;

import javax.inject.Inject;

/**
 * Created by alan on 01/09/2016.
 */
public class ResultsPresenter {

    private final static String TAG = "RR.ResultsPresenter";

    @Inject
    ResultsConnectorEmail resultsConnector;
    ResultsConnector.SigninListener signinListener;

    private FirebaseDatabase firebaseDatabase;

    private ResultsFragment resultsFragment;

    private String athleteName;

    @Inject public ResultsPresenter() {
    }

    public void onCreate(ResultsFragment resultsFragment) {
        this.resultsFragment = resultsFragment;

        athleteName = resultsFragment.getString(R.string.example_results_athlete_name);
    }

    public void onStart() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        EventBus.getDefault().register(this);
    }

    public void onStop() {

        EventBus.getDefault().unregister(this);
        firebaseDatabase = null;
    }

    public void onResume() {
        signinListener = new ResultsConnector.SigninListener() {
            @Override
            public void onUserSignedIn(FirebaseUser firebaseUser) {
                singleCompetitorAccess();
            }
        };
        resultsConnector.addSigninListener(signinListener);
    }

    public void onPause() {
        if (signinListener != null) {
            resultsConnector.removeSigninListener(signinListener);
        }
        signinListener = null;
        removeAthleteRef();
    }

    /**
     * Access the values for all the competitors
     *
     * <p>
     *     Example of some code called when we have been signed in.
     * </p>
     */
    private void allCompetitorsAccess() {

        DatabaseReference competitorKeysRef = DBRef.COMPETITOR_KEYS.ref(firebaseDatabase);
        competitorKeysRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Object value = dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "Could not use DB reference. DB access problem: " + databaseError);
            }
        });
    }

    AthleteKey athleteKey = new AthleteKey(DBRef.COMPETITORS);
    DatabaseReference athleteRef;
    ValueEventListener athleteEventListener;

    private void removeAthleteRef() {
        if (athleteRef != null && athleteEventListener != null) {
            athleteRef.removeEventListener(athleteEventListener);
            athleteRef = null;
            athleteEventListener = null;
        }
        athleteKey.cancel();
    }

    private void singleCompetitorAccess() {

        resultsFragment.setBusy(true);
        athleteKey.listen(firebaseDatabase, athleteName, new AthleteKey.AccessListener() {
            @Override
            public void referenceReceived(DatabaseReference newAthleteRef) {

                athleteRef = newAthleteRef;
                if (athleteRef == null) {
                    Log.e(TAG, "Could not find athlete reference for athlete: " + athleteName);
                    return;
                }
                Log.d(TAG, "Athlete reference: " + athleteRef);
                athleteEventListener = new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        GenericTypeIndicator<Map<String, CompetitorRace>> type =
                                new GenericTypeIndicator<Map<String, CompetitorRace>>() {
                                };
                        resultsFragment.setBusy(false);
                        competitorResults(dataSnapshot.getValue(type));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "Could not use DB reference. DB access problem: " + databaseError);
                        resultsFragment.setBusy(false);
                    }
                };
                athleteRef.addValueEventListener(athleteEventListener);

            }

            @Override
            public void referenceUnavailable() {
                Log.e(TAG, "Athlete reference unavailable");
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ResultMessageEvent event) {
        updateRecent(event);
    }

    /**
     * Receive a notification which tells us the most recent result.
     * <p>
     *     Tell the fragment to highlight the appropriate label.
     * </p>
     * @param event
     */
    private void updateRecent(ResultMessageEvent event) {
        String label = event.getLabel();
        if (!TextUtils.isEmpty(label)) {
            resultsFragment.setRecent(label);
        }
    }

    /**
     * These are the results for the competitor we have chosen to display.
     *
     * @param results
     */
    private void competitorResults(Map<String, CompetitorRace> results) {

        if (results == null) {
            return;
        }

        Log.d(TAG, "Results: " + results.size());

        //Push the new results into the display
        resultsFragment.setResults(results.values());

        // Check for an event that was delivered when we were idle
        // Use it to update the label of the most recent event.
        ResultMessageEvent event = EventBus.getDefault().getStickyEvent(ResultMessageEvent.class);
        if (event != null) {
            EventBus.getDefault().removeStickyEvent(ResultMessageEvent.class);
            updateRecent(event);
        }
    }

}
