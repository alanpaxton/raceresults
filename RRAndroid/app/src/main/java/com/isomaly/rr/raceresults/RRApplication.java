package com.isomaly.rr.raceresults;

import android.app.Application;
import android.content.pm.PackageManager;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import javax.inject.Inject;

/**
 * Created by alan on 16/07/2016.
 */
public class RRApplication extends Application {

    RRComponent component;

    @Inject NotificationState notificationState;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerRRComponent.builder().build();

        getComponent().inject(this);
        notificationState.checkAndUpdate(this);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    public RRComponent getComponent() {
        return component;
    }

}
