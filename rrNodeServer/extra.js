/**
 * Created by alan on 30/08/2016.
 */

var logger = require('./logger');
logger.debugLevel = 'info';
var uploader = require('./uploader');
uploader.initialize();
uploader.parseResults('Selkirk 2016 stuweb_results-4833.csv', 'Selkirk 2016', ',');

