/**
 * Created by alan on 30/06/2016.
 */

var logger = require('./logger');
logger.debugLevel = 'info';
var uploader = require('./uploader');
uploader.initialize();
uploader.clearResults();
uploader.parseResults('Results Lochore 2016.csv', 'Lochore Bruce 2016', ',');
uploader.parseResults('FoxTrail WRS 2015_2016_ Race Five, 13k.txt', 'Foxtrail 2015_16 Race 5', '\t');
uploader.parseResults('Lochore (Bruce) 2015 Results.csv', 'Lochore Bruce 2015', ',');
uploader.parseResults('Selkirk 2016 stuweb_results-4833.csv', 'Selkirk 2016', ',');
uploader.parseResults('Gullane_triathlon_2016_-_Final_results.csv', 'Gullane 2016', ',');