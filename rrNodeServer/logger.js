/**
 * Created by alan on 30/06/2016.
 */

var logger = exports;
logger.debugLevel = 'warn';
logger.log = function(level, message) {
    var levels = ['error', 'warn', 'info', 'verbose'];
    if (levels.indexOf(level) <= levels.indexOf(logger.debugLevel) ) {
        if (typeof message !== 'string') {
            message = JSON.stringify(message);
        };
        console.log(level+': '+message);
    }
}
