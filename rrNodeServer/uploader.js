/**
 * Created by alan on 22/06/2016.
 */

const parse = require('csv-parse');
const firebase = require("firebase");
const fs = require('fs');
const https = require('https');
const logger = require('./logger');

var uploader = exports;

/**
 * Start the uploader; hook up to firebase.
 *
 * Also start the listener which processes added or changed sets of results,
 * and responds by updating all the users based on these results.
 *
 * We may initiate an unncessary update (and user visible notification)
 * when we upload changed versions of a set of results; that doesn't seem too terrible,
 * and would take a lot of work (checksum of each user result ?) to rectify.
 *
 * The more obvious current version of the problem is that we will get re-notified when we restart
 * the server. We need some kind of
 */
uploader.initialize = function() {

    firebase.initializeApp({
        serviceAccount: "RaceResults-d5ea39f270cf.json",
        databaseURL: "https://raceresults-94c60.firebaseio.com"
    });

    uploader.db = firebase.database();
    uploader.startTime = Date.now();

    /**
     * Watch and log changes, either from initial creation or update.
     */

    var resultsRef = uploader.db.ref().child('results');
    const processResults = function (tag, snapshot) {
        var result = snapshot.val();
        logger.log('info', 'RR results ' + tag + ' ' + snapshot.ref + ', ' + result.label + ', ' + Object.keys(result.values).length + ' rows, ' + result.rawheader);
        logger.log('info', 'RR results fields ' + JSON.stringify(result.fields));
        if (uploader.startTime < result.resultTime) {
            updateCompetitors(snapshot)
        } else {
            logger.log('info', 'RR results ' + result.label + ' passed on update, result was created before restart ' + result.resultTime + ',' + uploader.startTime);
        }
    };
    resultsRef.on("child_added", function (snapshot) {
        processResults("child_added", snapshot);
    });
    resultsRef.on("child_changed", function (snapshot) {
        processResults("child_changed", snapshot);
    });

    uploader.serverKey = fs.readFileSync('SERVERKEY',encoding='utf8');
    logger.log('verbose', 'SERVERKEY ' + uploader.serverKey);
}

/**
 * Clear all results. Retain the competitor keys for existing competitors.
 *
 * This is setup for reloading the results from scratch, while keeping user-ids the same.
 */
uploader.clearResults = function () {
    uploader.db.ref().update({results: {}, resultKeys: {}, competitors: {}});
}

/**
 * Clear all results.
 *
 * Generally a VERY bad idea to do this, even when testing/developing,
 * and even if you think you know what you're doing (comment from experience).
 * The main problem is that this will destroy the UIDs of the users...........
 */
uploader.clearEverything = function () {
    uploader.db.ref().update({results: {}, resultKeys: {}, competitors: {}, competitorKeys: {}, competitorSettings : {}});
}

/**
 * Add the results/resultName/({fields:..., values:...}) entry.
 *
 * This is the "raw" list of results for a race,
 * before it has been processed for individual results.
 *
 * @param resultName
 * @param fields
 * @param values
 */
uploader.addResults = function(label, header, values) {
    var resultKeysRef = uploader.db.ref('resultKeys');
    var resultsRef = uploader.db.ref('results');
    var fieldMap = resultsDescriptor(header);
    const resultData = {label: label, rawheader: header, fields: fieldMap, values: values, resultTime: Date.now()};
    resultKeysRef.once('value', function (resultsKeySnapshot) {
        var key = null;
        const resultKeys = resultsKeySnapshot.val();
        if (resultKeys) {
            key = resultKeys[label];
        }

        if (!key) {
            //Push the new set of results, and stash the corresponding key
            const newkey = resultsRef.push(resultData).key;
            resultKeysRef.child(label).set(newkey);
        } else {
            //Update the old set of results, where the old key pointed us
            const oldref = resultKeysRef.child(key);
            oldref.set(resultData);
        }
    });
}

/**
 * Parse the set of results contained in a file.
 * When we have the parse results, add them to Firebase.
 *
 * An improvement would be to supply the completion function
 * Which receives the results (and does what it chooses).
 *
 * @param filename
 * @param label
 * @param delimiter
 */
uploader.parseResults = function(filename, label, delimiter) {

    var parser = parse({delimiter: delimiter, relax_column_count: true}, function (err, data) {
        if (err) {
            logger.log('error', 'RR parser error: ' + err.message);
        } else {
            if (data.length > 0) {
                var header = data[0];
                var contents = data.slice(1);
                uploader.addResults(label, header, contents);
            }
        }
    });
    fs.createReadStream(filename).pipe(parser);
}

/**
 * Analyse the header row.
 *
 * Yield a dictionary from the standard names of the fields to its index in the result row.
 * So, for instance, we will find that the overallTime is column number 7 in these results.
 *
 * @param header
 */
function resultsDescriptor(header) {
    //These are the potential values we might see for each of the fields we seek.
    var headerOptions = {
        name: ['name','athlete name'],
        firstname: ['firstname', 'first name'],
        lastname: ['lastname', 'last name'],
        gender: ['gender'],
        age: ['age'],
        category: ['category', 'AG', 'agegroup', 'age group'],
        club: ['club'],
        time: ['overall', 'time', 'gun time'],
        pos: ['pos', 'place', 'position'],
        number: ['number', 'bib', 'bib no', 'bib number', 'race no', 'no','bib #'],
        swim: ['swim'],
        bike: ['bike', 'cycle'],
        run: ['run'],
        t1: ['t1', 'transition1', 'transition 1'],
        t2: ['t2', 'transition2', 'transition 2'],
        catpos: ['catpos', 'categorypos', 'category pos', 'cat pos'],
        genderpos: ['genderpos', 'genpos', 'gender pos', 'gen pos'],
        tod: ['TOD', 'timeofday', 'time of day']
    }

    //Build the reverse map of the results descriptor. i.e. {'bib number':'number', 'bib no':'number'...}
    var labelToField = {};
    Object.keys(headerOptions).forEach(function (option) {
        headerOptions[option].forEach(function (alt) {
            labelToField[alt.toLowerCase()] = option;
        });
    });

    //Build the thing that tells us the index of the fields in my terms.
    var fieldMap = {};
    var unknownFields = [];
    var mapped = '';
    for (var index in header) {
        var label = header[index].toLowerCase();
        if (labelToField.hasOwnProperty(label)) {
            fieldMap[labelToField[label]] = index;
            if (mapped.length > 0) {
                mapped = mapped + ', ';
            }
            mapped = mapped + label + ' -> ' + labelToField[label] + ' @ ' + index;
        } else {
            unknownFields.push(label);
        }
        index++;
    }
    logger.log('info', 'RR field map:' + mapped);
    if (unknownFields.length > 0) {
        logger.log('warn', 'RR ' + unknownFields.length + ' unknown field(s): ' + unknownFields);
    }

    return fieldMap;
}

/**
 * Update competitors by the results of a race
 *
 * All the competitors will have the result(s) for this race (re)set.
 *
 * @param snapshot
 */
function updateCompetitors(snapshot) {
    logger.log('info', 'RR update competitors for race w/key: ' + snapshot.key);

    //Wrap the race information with helpers
    var race = new Race(snapshot);

    //Fetch the competitor keys
    //Use them while adding results
    var competitorKeysRef = uploader.db.ref().child('competitorKeys');
    var competitorsRef = uploader.db.ref().child('competitors');
    competitorKeysRef.once('value', function (snapshot) {
        var competitorKeys = snapshot.val();
        race.values.forEach(function (result) {
            var name = result[race.fields.name];
            if (name) {
                var key = null;
                if (competitorKeys) {
                    key = competitorKeys[name];
                }
                if (!key) {
                    //Create the new key
                    logger.log('verbose', 'RR create a new key for ' + name);
                    key = competitorsRef.push().key;
                    competitorKeysRef.child(name).set(key);
                }
                var competitor = new Competitor(name, key);
                competitor.addResult(race, result);
            } else {
                logger.log('error', 'RR result needs a name ' + result);
            }
        });
    });
}

/**
 * The race object knows how the mapping from fields to indices works,
 * and can therefore get a field from the result row.
 *
 * @param snapshot
 * @constructor
 */
function Race(snapshot) {

    var race = snapshot.val();
    this.key = snapshot.key;
    this.label = race.label;
    this.fields = race.fields;
    this.values = race.values;
    this.fieldNames = function () {
        return Object.keys(this.fields);
    }
    this.getResult = function (fieldName, result) {
        return result[this.fields[fieldName]];
    }

}

/**
 * A competitor (holds their name and key)
 *
 * Knows how to generate competitor-structured results for a race
 *
 * @param name
 * @param key
 * @constructor
 */
function Competitor(name, key) {
    this.name = name;
    this.key = key;

    /**
     * Add the result to the DB, for this race for this competitor.
     * It may be a duplicate / update, or it may be a fresh result.
     *
     * @param race
     * @param result
     */
    this.addResult = function (race, result) {
        var competitorRef = uploader.db.ref().child('competitors').child(key);
        var myResultRef = competitorRef.child(race.key);
        var resultValue = {};
        race.fieldNames().forEach(function (fieldName) {
            resultValue[fieldName] = race.getResult(fieldName, result);
        });
        var entry = {result: resultValue, label: race.label};
        myResultRef.set(entry)
            .then(function () {
                logger.log('verbose', 'RR added competitor result ' + JSON.stringify(entry));
                uploader.racenotification(key, race.label);
            })
            .catch(function () {
                logger.log('error', 'RR failed to add competitor result');
            });
    }
}

/**
 * Check whether we have a notification token for a user.
 * If we find one, we can (and will try to) send them a notification.
 *
 * @param competitorKey
 * @param label
 */
uploader.racenotification = function(competitorKey, label) {
    var myTokenRef = uploader.db.ref().child('competitorSettings').child(competitorKey).child('notificationAccessToken');
    myTokenRef.once("value", function (snapshot) {
        var token = snapshot.val()
        if (token) {
            logger.log('verbose', 'RR found notification token for key ' + competitorKey + ' ,value ' + snapshot.val());
            uploader.notifyuser(token, label)
        }
    });
}

/**
 * Post a GCM notification to a user, about a result.
 *
 * @param notificationToken
 * @param label
 * @param result
 */
uploader.notifyuser = function(notificationToken, label, result) {

    const post_content_object = {
        'to': notificationToken,
        'notification': {
            'title' : 'New results for ' + label,
            'body' : 'Please take a look'
        },
        'data' : {
            'label' : label
        }
    }
    const post_content = JSON.stringify(post_content_object);
    const post_options = {
        hostname: 'fcm.googleapis.com',
        port: 443,
        path: '/fcm/send',
        method: 'POST',
        inputEncoding: 'utf8',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : 'key=' + uploader.serverKey,
            'Content-Length': Buffer.byteLength(post_content)
        }
    };

    // Set up the request
    var post_req = https.request(post_options);
    var body = '';
    post_req.on('error', function (e) {
        logger.log('error', 'posting notification: ' + e.message);
    });

    post_req.on('response', function(response) {
        if (response.statusCode == 200 && response.statusMessage == 'OK') {

            response.on('data', function (chunk) {
                body += chunk;
            })

            response.on('end', function() {
                // all data has been downloaded
                const post_response = JSON.parse(body);
                if (!post_response.success) {
                    logger.log('error','notification response ' + post_response);
                }
                logger.log('info', 'response receipt ended OK: ' + body);
            });
        } else {
            logger.log('error','notification response ' + response.statusCode + ' ' + response.statusMessage);
        }
    });

    // post the data
    post_req.end(post_content);
    logger.log('verbose', 'posted notification');
}
